// Опишіть, як можна створити новий HTML тег на сторінці.
// Спочатку отримуємо доступ до батьківського елемента
// let parentElement = document.querySelector('body')
// створюємо новий елемент 
// let newElement = document.createElement('p');
// можна налаштувати атрибути та вміст елементу
// додаємо новий елемент до батьківського елемента
// parentElement.appendChild(newElement);

// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Визначає місце, куди буде вставлений HTML-код. Варіанти: "beforebegin", "afterbegin", "beforeend", "afterend"

// Як можна видалити елемент зі сторінки? 
// За допомогою методу remove()

// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.
// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

function displayArrayAsList(array, parent = document.body) {
    const list = document.createElement('ul');
    
    for (const item of array) {
      const listItem = document.createElement('li');
      listItem.textContent = item;
      list.appendChild(listItem);
    }
    
    parent.appendChild(list);
  }
  
  const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
  displayArrayAsList(array);
